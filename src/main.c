#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include "config.h"
#include "commands.h"

#define ICICLE_DEFAULT_ROOT "/var/icicle"

static const char *SHORT_OPTIONS = "hr:p";

static const struct option LONG_OPTIONS[] = {
    {"help", no_argument, NULL, 'h'},
    {"root", required_argument, NULL, 'r'},
    {"pretend", no_argument, NULL, 'p'},
    {0, 0, 0, 0}
};

static const char *HELP =
"Usage:\n\
 %1$s [options...] <command> [arguments...]\n\
\n\
A simple game server supervisor.\n\
\n\
Options:\n\
 -h, --help         display this help and exit\n\
 -p, --pretend      allow to run as non-root but fail when actually starting things\n\
 -r, --root <root>  the root in which the socket should be found\n\
\n\
Commands:\n\
 daemon             run a daemon\n\
";

static const char *icicle_root;
static int _pretending = 0;

int main(int argc, char *argv[]) {

    icicle_root = ICICLE_DEFAULT_ROOT;

    int opt;
    while (1) {
        int option_index = 0;
        opt = getopt_long(argc, argv, SHORT_OPTIONS,
                          LONG_OPTIONS, &option_index);

        // No more options
        if (opt == -1) break;
        switch (opt) {
            case 'h':
                // Show help message
                printf(HELP, argv[0]);
                exit(EXIT_SUCCESS);
                break;
            case 'p':
                // Pretend
                _pretending = 1;
                break;
            case 'r':
                // Use a special root
                icicle_root = optarg;
                break;
            case '?':
                // Error message
                exit(EXIT_FAILURE);
                break;
        }

    }
    printf("icicle root = %s\n", icicle_root);
    process_command(argv+optind, -1);

}

const char *get_root() {
    return icicle_root;
}

const char *get_help() {
    return HELP;
}

int pretending() {
    return _pretending;
}
