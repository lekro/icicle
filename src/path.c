#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * Join the path components given in the args.
 * The args must be terminated with a null pointer.
 */
char *join_path(const char *args[]) {
    int len = 0;
    for (int i = 0; args[i]; i++) {
        if (!args[i]) continue;
        if (!args[i][0]) continue;
        int arglen = strlen(args[i]);
        len += arglen;
        if (args[i][arglen-1] != '/') {
            len += 1;
        }
    }

    char *joined = malloc((len+1) * sizeof(char));
    if (joined == NULL) {
        fputs("Could not allocate space for joined path", stderr);
        return NULL;
    }

    char *curr = joined;
    for (int i = 0; args[i]; i++) {
        // Copy the path component and set curr to the null terminator.
        curr = stpcpy(curr, args[i]);
        if (curr == joined) continue;
        if (curr[-1] != '/' && args[i+1]) {
            // We didn't find a slash, so add one.
            *(curr++) = '/';
            *curr = '\0';
        }
    }

    return joined;

}

