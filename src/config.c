#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>

#include "config.h"
#include "path.h"
#include "main.h"


static const struct config_option CONFIG_OPTIONS[] = {
    // Executable to run. Only used if construct_java_args == 0.
    {"exec", NULL, CONFIG_STRING},

    // Arguments to supply to executable. Only used if construct_java_args == 0.
    {"args", NULL, CONFIG_STRING},

    // Should we construct java arguments by looking at other configuration values?
    {"construct_java_args", "true", CONFIG_BOOLEAN},

    // Path to java executable
    {"java_path", "/usr/bin/java", CONFIG_STRING},

    // Add these extra args to the end of all the JVM args
    {"java_jvm_args", "", CONFIG_STRING},

    // Execute a jar
    {"java_use_jar", "true", CONFIG_BOOLEAN},

    // Program path
    {"java_program_path", "server.jar", CONFIG_STRING},

    // Add these extra args to the java program itself
    {"java_program_args", "", CONFIG_STRING},

    // Minimum and maximum RAM, in megabytes
    {"java_min_ram", "1024", CONFIG_INTEGER},
    {"java_max_ram", "1024", CONFIG_INTEGER},

    // Stack size in kilobytes
    {"java_stack_size", "1024", CONFIG_INTEGER}

};

static const int CONFIG_OPTION_COUNT = sizeof(CONFIG_OPTIONS) / sizeof(struct config_option);

struct file_config {
    // The server to which this configuration pertains
    char *name;

    // The actual array of configuration values
    struct config_value *values;

    // This is a linked list and we are thus storing the pointer to the next element.
    struct file_config *next;
};

struct file_config *configs = NULL;
pthread_mutex_t config_mut;

void config_lock() {
    pthread_mutex_lock(&config_mut);
}

void config_unlock() {
    pthread_mutex_unlock(&config_mut);
}

int get_config_option_count() {
    return CONFIG_OPTION_COUNT;
}

void init_configs() {
    pthread_mutex_init(&config_mut, NULL);
}


/*
 * Get the config option with the given name.
 * If no such option exists, return NULL.
 *
 * name - a null-terminated string with the name of the config option
 */

/*
static const struct config_option *get_option(const char *name) {

    for (int i = 0; i < CONFIG_OPTION_COUNT; i++) {
        if (strcmp(CONFIG_OPTIONS[i].name, name) == 0) {
            return &(CONFIG_OPTIONS[i]);
        }
    }

    return NULL;
}
*/

/*
 * Get the path to the config file in which the requested configuration
 * resides or would reside.
 * The path is allocated in the heap, so it is the caller's responsibility
 * to free() it later.
 *
 * name - a null-terminated string with the server name, or NULL
 *        to get the global configuration
 */
char *get_config_path(const char *name) {

    if (name) {
        const char *pieces[] = {get_root(), name, "icicle.conf", NULL};
        char *joined = join_path(pieces);
        return joined;
    } else {
        const char *pieces[] = {get_root(), "icicle.conf", NULL};
        char *joined = join_path(pieces);
        return joined;
    }

}

/*
 * Get an array of config_values which are filled with their default
 * values
 */
struct config_value *get_default_config() {
    struct config_value *values = malloc(CONFIG_OPTION_COUNT * sizeof(struct config_value));
    if (values == NULL) {
        fputs("Could not allocate memory for default configuration", stderr);
        return NULL;
    }

    for (int i = 0; i < CONFIG_OPTION_COUNT; i++) {
        values[i] = (struct config_value) {CONFIG_OPTIONS[i], NULL};
    }
    return values;
}

/*
 * Free all the memory taken by the config, including heap-allocated values.
 */
void free_config(struct config_value *config) {
    for (int i = 0; i < CONFIG_OPTION_COUNT; i++) {
        if (config[i].value != NULL) {
            free(config[i].value);
        }
    }
    free(config);
}

/*
 * In an array of config_values, check if the given key exists.
 */
int config_has_value(const struct config_value *config, const char *key) {

    pthread_mutex_lock(&config_mut);
    
    for (int i = 0; i < CONFIG_OPTION_COUNT; i++) {
        if (strcmp(config[i].option.name, key) == 0) {
            pthread_mutex_unlock(&config_mut);
            return true;
        }
    }
    pthread_mutex_unlock(&config_mut);
    return false;
}

/*
 * In an array of config_values, set the value of the given key.
 * Passing in NULL basically unsets it.
 * The value used is a copy of the value passed in.
 */
int config_set_value(struct config_value *config, const char *key, const char *value) {

    pthread_mutex_lock(&config_mut);

    for (int i = 0; i < CONFIG_OPTION_COUNT; i++) {

        bool invalid_value = false;

        if (strcmp(config[i].option.name, key) == 0) {
            // Check type of the new value.
            switch (config[i].option.type) {
                case CONFIG_BOOLEAN:
                    if (strcmp(value, "true") && strcmp(value, "false")) {
                        fprintf(stderr, "Ignoring invalid boolean '%s' for key %s in config.\n",
                                value, key);
                        invalid_value = true;
                    }
                    break;
                case CONFIG_INTEGER:
                    errno = 0;
                    char *errptr = NULL;
                    strtol(value, &errptr, 10);
                    if (errno != 0) {
                        fprintf(stderr, "Ignoring invalid integer '%s' for key %s in config: ",
                                value, key);
                        perror(NULL);
                        invalid_value = true;
                    }
                    if (errptr && *errptr) {
                        fprintf(stderr, "Ignoring invalid integer '%s' for key %s in config\n",
                                value, key);
                        invalid_value = true;
                    }

                    break;
                case CONFIG_STRING:
                default:
                    // no type checking
                    break;
            }

            if (invalid_value) continue;

            char *newval = malloc((strlen(value)+1) * sizeof(char));
            if (newval == NULL) {
                return false;
            }

            strcpy(newval, value);
            config[i].value = newval;

            pthread_mutex_unlock(&config_mut);
            return true;
        }
    }
    
    pthread_mutex_unlock(&config_mut);
    return false;

}

/*
 * In an array of config_values, get the value of the given key,
 * returning the default value if none is defined.
 */
const char *config_get_value(const struct config_value *config, const char *key) {

    const char *ret = NULL;
    pthread_mutex_lock(&config_mut);

    for (int i = 0; i < CONFIG_OPTION_COUNT; i++) {
        if (strcmp(config[i].option.name, key) == 0) {
            if (config[i].value == NULL) {
                ret = config[i].option.default_value;
            } else {
                ret = config[i].value;
            }
            break;
        }
    }

    pthread_mutex_unlock(&config_mut);
    return ret;

}

/*
 * In an array of config_values, get the value at the given index,
 * returning the default value if none is defined.
 */
const char *config_get_value_index(const struct config_value *config, const int i) {

    if (i >= CONFIG_OPTION_COUNT) return NULL;

    const char *ret;

    if (config[i].value == NULL) {
        ret = config[i].option.default_value;
    } else {
        ret = config[i].value;
    }

    return ret;
}

/*
 * Return a pointer to the requested array of config values, or NULL if
 * the requested configuration is not loaded.
 */
struct config_value *get_config(const char *name) {

    struct file_config *ptr = configs;
    while (ptr) {
        if (name == ptr->name || (!name && strcmp(name, ptr->name) == 0)) {
            // Found the configuration
            return ptr->values;
        }
        ptr = ptr->next;
    }
    return NULL;
}

struct config_value *get_config_eager(const char *name) {
    struct config_value *config;

    config = get_config(name);
    if (config == NULL) {
        if (load_config(name) != 0) {
            return NULL;
        } else {
            config = get_config(name);
        }
    }

    return config;
}

/*
 * Overwrite the requested array of config values with the given array.
 */
void set_config(const char *name, struct config_value *values) {

    // First, try to find the name in the already-loaded configuration.
    struct file_config *ptr = configs;
    struct file_config *prev = ptr;
    while (ptr) {
        if (name == ptr->name || (!name && strcmp(name, ptr->name) == 0)) {
            // Found the configuration. Overwrite.
            free_config(ptr->values);
            ptr->values = values;

            return;
        }
        prev = ptr;
        ptr = ptr->next;
    }

    // If we are here, that means we should add a new configuration.
    struct file_config *fconf = malloc(sizeof(struct file_config));
    if (fconf == NULL) {
        fputs("Could not allocate file_config struct", stderr);
        return;
    }

    if (name) {
        fconf->name = malloc(sizeof(char) * (strlen(name) + 1));
        strcpy(fconf->name, name);
    } else {
        fconf->name = NULL;
    }
    fconf->next = NULL;
    fconf->values = values;
    
    if (prev) {
        prev->next = fconf;
    } else {
        configs = fconf;
    }

}


/*
 * Diagnostic print the configuration.
 */
void print_config(const struct config_value *config) {

    if (config == NULL) {
        puts("Empty configuration");
    } else {
        puts("# Configuration");
        for (int i = 0; i < CONFIG_OPTION_COUNT; i++) {
            printf("%s = '%s' (default '%s')\n",
                   config[i].option.name,
                   config_get_value(config, config[i].option.name),
                   config[i].option.default_value);
        }
    }

}

// Preprocessor sorcery to automatically supply fscanf string length
// limits from the following definitions.
// We could, of course, simply snprintf these into a new format string
// and use that with an equivalent result, but this avoids doing that at
// runtime.
#define CONFIG_KEY_MAX 31
#define CONFIG_VAL_MAX 255
#define STR(x) #x
#define XCONFIG_SCAN_FORMAT(a, b) ("%" STR(a) "s = %" STR(b) "s\n")
#define CONFIG_SCAN_FORMAT XCONFIG_SCAN_FORMAT(CONFIG_KEY_MAX, CONFIG_VAL_MAX)

/*
 * Load the config for the server with the given name.
 * If the config file already exists, it will be force-reloaded
 * and any data in memory will be overwritten.
 *
 * The "name" given is the name of a subdirectory in which the 
 * server resides
 */
int load_config(const char *name) {

    if (name && *name == '\0') {
        return -1;
    }
    char *config_path = get_config_path(name);
    errno = 0;
    FILE *file = fopen(config_path, "r");
    if (errno != 0) {
        perror(config_path);
        free(config_path);
        return -1;
    }

    int n;
    struct config_value *values = get_default_config();
    if (values == NULL) {
        return -1;
    }

    char key[CONFIG_KEY_MAX];
    char value[CONFIG_VAL_MAX];

    do {
        // Check if there is a comment
        unsigned char next_char = fgetc(file);
        if (next_char == EOF) break;
        if (next_char == '#') {
            // skip this line
            fscanf(file, "%*s\n");
            continue;
        }
        fseek(file, -1, SEEK_CUR);

        errno = 0;
        n = fscanf(file, CONFIG_SCAN_FORMAT, key, value);

        if (errno != 0) {
            perror(config_path);
            free(config_path);
            fclose(file);
            return -1;
        }

        if (n == 2) {
            // Correct format for configuration.
            //printf("Key: %s // Value: %s\n", key, value);

            if (!config_has_value(values, key)) {
                fprintf(stderr, "Ignoring unknown key '%s' in config file %s.\n", key, config_path);
                continue;
            } else {
                config_set_value(values, key, value);
            }

        } else if (n != -1) {
            fprintf(stderr, "Invalid format in config file %s. Got %d args\n", config_path, n);
            free(config_path);
            fclose(file);
            return -1;
        }

    } while (n != -1);

#ifdef _DEBUG
    print_config(values);
#endif

    fclose(file);
    free(config_path);

    // Put the pointer to the configuration.
    set_config(name, values);
    return 0;

}

int save_config(const char *name) {

    // Make sure we actually have such a configuration loaded.
    struct config_value *values;
    if ((values = get_config(name)) == NULL) {
        // If we couldn't find it, use the default.
        values = get_default_config();
    }

    char *config_path = get_config_path(name);
    errno = 0;
    FILE *file = fopen(config_path, "w");
    if (errno != 0) {
        perror(config_path);
        free(config_path);
        exit(EXIT_FAILURE);
    }

    // Actually save the configuration here.
    for (int i = 0; i < CONFIG_OPTION_COUNT; i++) {
        const char *value = config_get_value_index(values, i);
        if (value == NULL) continue;
        fprintf(file, "%s = %s\n", values[i].option.name, value);
    }

    fclose(file);
    free(config_path);
    return 0;

}

    



