#include "../proto.h"
#ifndef ICICLE_CLIENT_PROTO_H
#define ICICLE_CLIENT_PROTO_H
const struct icicle_proto *get_client_proto();
int send_string(int sock, char cmd, const char *buf);
int send_error(int sock, const char *buf);
int send_message(int sock, const char *buf);
#endif
