#define _GNU_SOURCE
#include <stdio.h>
#include <stddef.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <pthread.h>

#include "../main.h"
#include "../path.h"
#include "../socket.h"
#include "../console.h"
#include "../commands.h"
#include "client_proto.h"

static int message_sock;
static pthread_t message_thread;

void append_arg(char *, size_t *, size_t *, char ***);


int send_data(void *arg, char *buf) {

    int sock = *((int *) arg);
    int nbytes = write(sock, buf, strlen(buf) + 1);
    if (nbytes < 0) {
        perror("write");
    }

    return 0;
}


void *client_listen(void *arg) {

    const int sock = *((int *) arg);
    char buf[256];
    const struct icicle_proto *handlers = get_client_proto();
    while (1) {
        int nbytes = read(sock, buf, 256);
        if (nbytes < 0) {
            perror("read");
            break;
        } else if (nbytes == 0) {
            puts("[Daemon closed the connection]");
            break;
        } else {
            handle_proto(handlers, buf, nbytes, NULL);
        }
    }

    close(sock);
    shutdown(sock, 2);
    return NULL;

}


int connect_socket(const char *socket_path) {

    int sock = socket(PF_LOCAL, SOCK_STREAM, 0);
    if (sock < 0) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_un *name = get_sockaddr(socket_path);
    size_t size = SUN_LEN(name);

    if (connect(sock, (struct sockaddr *) name, size) < 0) {
        free(name);
        fprintf(stderr, "binding to %s: ", socket_path);
        perror(NULL);
        return -1;
    }

    free(name);

    // spawn a thread to listen
    message_sock = sock;
    pthread_create(&message_thread, NULL, client_listen, &message_sock);
    return sock;

}
    


int client_shell(int s) {

    if (s >= 0) {
        fputs("error: Cannot run a shell within itself\n", stderr);
        return -1;
    }

    char *socket_path = get_socket_path();
    int sock = connect_socket(socket_path);
    free(socket_path);

    if (sock < 0) {
        return -1;
    }

    // Show the shell
    puts("Welcome to icicle's shell. (^D to exit)");
    char *buf = NULL;
    size_t n = 0;
    char **args = NULL;

    while (getline(&buf, &n, stdin) >= 0) {

        // Remove trailing newline...
        char *end = strchrnul(buf, '\n');
        *end = '\0';

        // Split into tokens...
        size_t nargs = 0;
        size_t argcap = 4;
        args = calloc(argcap, sizeof(*args));
        if (args == NULL) {
            fputs("error: Could not allocate memory for token array", stderr);
            continue;
        }
        
        *args = NULL;

        // Use a state machine as given in https://stackoverflow.com/questions/26187037
        char state = ' ';
        char *bptr = buf;
        while (*bptr) {
            switch (state) {
                case ' ':
                    if (*bptr == '"') {
                        // Begin quote
                        state = 'q';
                    } else if (*bptr != ' ') {
                        // We are past the space and should use this
                        append_arg(bptr, &nargs, &argcap, &args);
                        state = 't';
                    }
                    break;
                case 't':
                    if (*bptr == ' ') {
                        // String ends
                        *bptr = '\0';
                        state = ' ';
                    }
                    break;
                case 'q':
                    append_arg(bptr, &nargs, &argcap, &args);
                    state = 'Q';
                    break;
                case 'Q':
                    if (*bptr == '"') {
                        *bptr = '\0';
                        state = ' ';
                    }
                    break;
            }
            bptr++;
        }

#ifdef _DEBUG
        char **a = args;
        while (*a) {
            printf("argument: '%s'\n", *a);
            a++;
        }
#endif

        process_command(args, sock);

        free(args);

    }

    pthread_cancel(message_thread);
    pthread_join(message_thread, NULL);
    free(buf);
    close(sock);
    shutdown(sock, 2);
    return 0;
}

void append_arg(char *arg, size_t *nargs, size_t *argcap, char ***args) {

    if (*nargs == *argcap) {
        // Amortized doubling
        *argcap *= 2;
        *args = reallocarray(*args, *argcap, sizeof(**args));
    }
    (*args)[(*nargs)++] = arg;
    (*args)[*nargs] = NULL;

}


