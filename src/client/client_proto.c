#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "client_proto.h"

int handler_error(char *data, size_t len, void *aux);
int handler_message(char *data, size_t len, void *aux);

struct icicle_proto client_proto[] = {
    {'m', handler_message},
    {'e', handler_error},
    {'\0', NULL}
};

const struct icicle_proto *get_client_proto() {
    return client_proto;
}

int handler_error(char *data, size_t len, void *aux) {
    fprintf(stderr, "error: %.*s\n", (int) len-1, data+1);
    return 0;
}

int handler_message(char *data, size_t len, void *aux) {
    fprintf(stdout, "%.*s\n", (int) len-1, data+1);
    return 0;
}

int send_string(int sock, char cmd, const char *buf) {
    size_t len = strlen(buf);
    char *nbuf = malloc(len + 2);

    if (nbuf == NULL) {
        fprintf(stderr, "error: could not allocate memory for buffer\n");
        return -1;
    }

    *nbuf = cmd;
    strcpy(nbuf+1, buf);
    int nbytes = write(sock, nbuf, len+2);
    if (nbytes < 0) {
        perror("write");
        return -1;
    }

    return 0;
}

int send_error(int sock, const char *buf) {
    return send_string(sock, 'e', buf);
}

int send_message(int sock, const char *buf) {
    return send_string(sock, 'm', buf);
}

