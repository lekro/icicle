#include <stdbool.h>

#ifndef ICICLE_CONFIG_H

/*
 * Types of configuration key-value pairs possible.
 * CONFIG_GLOBAL means the pair applies only to the global configuration.
 * CONFIG_LOCAL means that the pair applies only to the local configuration,
 * but values can be set in the global configuration to provide default values
 * for local configurations.
 *
 * For example, if this is really just a single-group system, you might as well
 * set the "group" option to your one group, and not specifying one in local
 * configuration files.
 */
enum config_locality {
    CONFIG_GLOBAL,
    CONFIG_LOCAL
};

/* 
 * Type to use to perform type-checking on configuration values.
 *
 * CONFIG_STRING accepts anything
 * CONFIG_BOOLEAN accepts only "true" or "false"
 * CONFIG_INTEGER accepts only things which are all numbers
 */
enum config_type {
    CONFIG_STRING,
    CONFIG_BOOLEAN,
    CONFIG_INTEGER
};

/*
 * A configuration option, with value.
 *
 * name             configuration key
 * default_value    a default value for the key
 * value            the actual value
 * locality         is this LOCAL or GLOBAL?
 * type             type of value, perform type checking
 * required         must there be a value? Usually not setting anything
 *                  anywhere makes more sense. For example, if you wish to
 *                  take advantage of builtin java argument construction,
 *                  you should probably leave "args" undefined.
 */
struct config_option {
    char *name;
    char *default_value;
    enum config_type type;
};

struct config_value {
    struct config_option option;
    char *value;
};

void config_lock(void);
void config_unlock(void);

int load_config(const char *name);
int save_config(const char *name);
void init_configs(void);
struct config_value *get_config(const char *);
struct config_value *get_config_eager(const char *);

int config_has_value(const struct config_value *config, const char *key);
int config_set_value(struct config_value *config, const char *key, const char *value);
const char *config_get_value(const struct config_value *config, const char *key);



#endif
