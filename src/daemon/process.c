#include <unistd.h>
#include <sys/types.h>

int upopen(const char **command, uid_t uid, gid_t gid) {

    pid_t pid = fork();

    if (pid == 0) {
        // Within child process
        // Remove supplementary groups, setgid, setuid
        setgroups(0, NULL);
        setgid(gid);
        setuid(uid);
        // Actually execute process
        int exec_status = execvp(command[0], command);
        // If the exec was successful, it won't return. Exit if it did.
        _exit(EXIT_FAILURE);
    } else {
        // Within parent process
        return pid;
    }

}
