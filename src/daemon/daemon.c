#define _GNU_SOURCE

#include <stddef.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <pthread.h>

#include "../main.h"
#include "../path.h"
#include "../socket.h"
#include "../config.h"
#include "../proto.h"
#include "../client/client_proto.h"
#include "daemon.h"
#include "daemon_proto.h"

#define CONN_MAX 256

// A stack holds threads.
static pthread_t threads[CONN_MAX];
static unsigned int threads_free[CONN_MAX];
static unsigned int threads_free_count = CONN_MAX;
static pthread_mutex_t threads_mut = PTHREAD_MUTEX_INITIALIZER;

static void init_threads() {
    for (unsigned int i = 0; i < CONN_MAX; i++) {
        threads_free[i] = i;
    }
}

/*
 * Get a free thread, or return NULL if none exists.
 */
static pthread_t *pop_thread() {
    pthread_t *ret;

    pthread_mutex_lock(&threads_mut);
    if (threads_free_count > 0) {
        int i = threads_free[--threads_free_count];
        ret = threads + i;
    } else {
        ret = NULL;
    }

    pthread_mutex_unlock(&threads_mut);
    return ret;
}

static void push_thread_nolock(pthread_t *thread) {
    threads[threads_free[threads_free_count++]] = thread - threads;
}
    

/*
 * Free a thread.
 */
static void push_thread(pthread_t *thread) {
    pthread_mutex_lock(&threads_mut);
    push_thread_nolock(thread);
    pthread_mutex_unlock(&threads_mut);
}

/*
 * Try to join and free memory relating to all threads which are popped.
 */
static void tryjoin_all() {
    pthread_mutex_lock(&threads_mut);
    for (int i = threads_free_count; i < CONN_MAX; i++) {
        struct client_info *info;
        if (pthread_tryjoin_np(threads[i], (void *) &info)) continue;
        // Need to free this thread...
        push_thread_nolock(info->thread);
        free(info);
    }
}
    

int get_socket(const char *socket_path) {

    // Start UNIX socket
    int sock = socket(PF_LOCAL, SOCK_STREAM, 0);
    if (sock < 0) {
        // Opening the socket failed
        perror("socket");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_un *name = get_sockaddr(socket_path);
    size_t size = SUN_LEN(name);

    if (pretending()) {
        // Delete the socket since we're testing
        if (unlink(socket_path) != 0) {
            if (errno != ENOENT) {
                fprintf(stderr, "deleting %s: ", socket_path);
                perror(NULL);
                exit(EXIT_FAILURE);
            }
        }
    }


    if (bind(sock, (struct sockaddr *) name, size) < 0) {
        fprintf(stderr, "binding to %s: ", socket_path);
        perror(NULL);
        exit(EXIT_FAILURE);
    }

    free(name);

    return sock;
}


void *handle_client(void *arg) {
    struct client_info *info = (struct client_info *) arg;
    int sock = info->sock;

    printf("new thread for connection %d\n", sock);

    // Actually handle the connection now...
    char buf[256];
    const struct icicle_proto *handlers = get_daemon_proto();
    while (1) {
        int nbytes = read(sock, buf, 256);
        if (nbytes < 0) {
            perror("read");
            break;
        } else if (nbytes == 0) {
            printf("EOF from %d\n", sock);
            break;
        } else {
            printf("from %d: %s\n", sock, buf);
            errno = 0;
            handle_proto(handlers, buf, nbytes, (void *) info);
            if (errno == EINVAL) {
                send_error(sock, "Unknown command");
            }
        }
    }

    close(sock);
    shutdown(sock, 2);

    return (void *) info;
}


void accept_incoming(int sock) {
    struct sockaddr addr;
    socklen_t len;
    struct ucred ucred;
    socklen_t ucred_len;

    memset(&addr, 0, sizeof(addr));
    memset(&len, 0, sizeof(len));
    memset(&ucred, 0, sizeof(ucred));
    ucred_len = sizeof(ucred);

    int fd = accept(sock, &addr, &len);
    if (fd < 0) {
        perror("socket accept");
        return;
    }

    if (getsockopt(fd, SOL_SOCKET, SO_PEERCRED, &ucred, &ucred_len) < 0) {
        perror("socket auth");
        close(fd);
        shutdown(fd, 2);
        return;
    }

    printf("connection accepted: %d\n", fd);
    printf("ucred len = %d\n", ucred_len);
    printf("credentials: pid=%ld, euid=%ld, egid=%ld\n",
            (long) ucred.pid, (long) ucred.uid, (long) ucred.gid);

    struct client_info *info = malloc(sizeof(*info));
    if (info == NULL) {
        fputs("Could not allocate space for client thread information", stderr);
        close(fd);
        shutdown(fd, 2);
        return;
    }

    info->sock = fd;
    info->uid = (long) ucred.uid;
    info->gid = (long) ucred.gid;
    info->thread = pop_thread();
    if (info->thread == NULL) {
        fprintf(stderr, "Maximum threads reached (%d)\n", CONN_MAX);
        close(fd);
        shutdown(fd, 2);
        return;
    }

    pthread_create(info->thread, NULL, handle_client, (void *) info);
}

int start_daemon(int s) {

    if (s >= 0) {
        fputs("error: Cannot start a daemon from icicle shell\n", stderr);
        return -1;
    }

    init_threads();

    // Load configuration
    config_lock();
    init_configs();
    load_config(NULL);
    config_unlock();

    // Check if we are root
    if (!pretending() && getuid() != 0) {
        fputs("error: Daemon must be started as root.\n", stderr);
        return 1;
    }

    // Get UNIX socket address
    char *socket_path = get_socket_path();

    // Open socket
    int sock = get_socket(socket_path);
    free(socket_path);

    // Accept incoming connections
    do {
        if (listen(sock, 16) < 0) {
            perror("socket listen");
            exit(EXIT_FAILURE);
        }
        accept_incoming(sock);
        //tryjoin_all();
    } while (1);

    close(sock);
    
    return 0;

}
