#include "../client/client_proto.h"
#include "daemon.h"
#include "daemon_proto.h"
#include "server.h"

#define HANDLER(name) int (name)(char *data, size_t len, void *aux)

//int handler_fix_permissions(char *data, size_t len, void *aux);
//int handler_create(char *data, size_t len, void *aux);
HANDLER(handler_fix_permissions);
HANDLER(handler_create);

struct icicle_proto daemon_proto[] = {
    {'p', handler_fix_permissions},
    {'c', handler_create},
    {'\0', NULL}
};

const struct icicle_proto *get_daemon_proto() {
    return daemon_proto;
}

int handler_fix_permissions(char *data, size_t len, void *aux) {
    // TODO check auth first
    struct client_info *info = (struct client_info *) aux;
    int retval = server_fix_permissions(data+1);
    if (retval == -1) {
        send_error(info->sock, "That server does not exist");
    } else {
        send_message(info->sock, "Successfully set permissions");
    }
    return retval;
}

int handler_create(char *data, size_t len, void *aux) {
    return 0;
}
