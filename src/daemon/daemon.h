#include <pthread.h>

#ifndef ICICLE_DAEMON_MAIN_H
#define ICICLE_DAEMON_MAIN_H

struct client_info {
    int sock;
    long uid;
    long gid;
    pthread_t *thread;
};

int start_daemon();

#endif
