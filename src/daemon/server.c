#define _XOPEN_SOURCE 500

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <ftw.h>
#include <errno.h>
#include "../config.h"
#include "../main.h"
#include "../path.h"

struct ftw_params {
    uid_t uid;
    gid_t gid;
    pthread_mutex_t mut;
};

static struct ftw_params ftw_params = {0, 0, PTHREAD_MUTEX_INITIALIZER};

static int fix_permissions_callback(const char *fpath, const struct stat *sb,
                                    int typeflag, struct FTW *ftwbuf) {
    puts(fpath);
    if (typeflag == FTW_D) {
        // Directory. Should be chmod 770 icicle:GROUP
        // Because the file tree walk doesn't allow us to pass any parameters,
        // those will be stored in a global variable :(
        // Because multiple threads could access that global variable, it will be
        // protected by a pthreads mutex.
    }
    return 0;
}


int server_fix_permissions(char *name) {

    printf("'%s'\n", name);
    struct stat stat_buf;
    struct config_value *config = get_config_eager(name);

    int retval;
    if (config == NULL) {
        return -1;
    }

    // Config is now loaded
    pthread_mutex_lock(&ftw_params.mut);

    puts("HI");
    const char *path_pieces[] = {get_root(), name, NULL};
    char *path = join_path(path_pieces);

    // Try to stat the directory of the server
    errno = 0;
    stat(path, &stat_buf);
    if (errno == ENOENT) {
        retval = -1;
    } else {
        retval = nftw(path, fix_permissions_callback, 4, 0);
    }
    free(path);
    pthread_mutex_unlock(&ftw_params.mut);

    return retval;
}


//int server_create(char *name, gid_t gid) {
    
