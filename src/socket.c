#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "socket.h"
#include "path.h"
#include "main.h"

struct sockaddr_un *get_sockaddr(const char *socket_path) {

    size_t size = sizeof(short int) + (strlen(socket_path)+1) * sizeof(char);
    struct sockaddr_un *name = malloc(size);
    if (name == NULL) {
        fputs("Could not allocate space for socket address", stderr);
        return NULL;
    }
    memset(name, 0, size);
    name->sun_family = AF_LOCAL;
    strcpy(name->sun_path, socket_path);

    return name;

}

char *get_socket_path() {
    const char *socket_join[] = {get_root(), "icicle.sock", NULL};
    char *socket_path = join_path(socket_join);
    return socket_path;
}

