#ifndef ICICLE_CONSOLE_H
#define ICICLE_CONSOLE_H

void show_console(int (*send_data)(void *, char *), void *data);

#endif
