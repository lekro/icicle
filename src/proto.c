#ifdef _DEBUG
#include <stdio.h>
#endif
#include <stddef.h>
#include <errno.h>
#include "proto.h"

// see comment in proto.h
int handle_proto(const struct icicle_proto *handlers, char *data, size_t len, void *aux) {

    if (len < 1) {
        // no command given
        return ERR_NOCMD;
    }

    while (handlers->label) {
        if (handlers->label == *data) {
#ifdef _DEBUG
            //printf("Matched protocol command '%c'\n", *data);
#endif
            return handlers->handler(data, len, aux);
        }
        handlers += 1;
    }

    errno = EINVAL;
    return ERR_UNKNOWN;

}
