#ifndef ICICLE_COMMANDS_H
#define ICICLE_COMMANDS_H

struct command {
    const char *name;
    const int(*func)(int sock);
};

void process_command(char *argv[], int sock);

#endif
