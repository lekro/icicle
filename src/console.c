#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <locale.h>
#include <ncurses.h>

#define INPUT_BUFFER_SIZE 256
#define INPUT_BUFFER_LIMIT (INPUT_BUFFER_SIZE - 1)
#define MIN(x, y) ((x < y) ? x : y)

WINDOW *create_tail_window(int h, int w);
WINDOW *create_input_window(int h, int w);
WINDOW *create_status_window(int h, int w);
void show_console();

/*
int main(int argc, char *argv[]) {

    setlocale(LC_ALL, "");
    show_console();
    return 0;

}
*/

void show_console(int (*send_data)(void *, char *), void *data) {

    WINDOW *tail, *input, *status;
    int height, width;
    int curr;

    char input_buffer[INPUT_BUFFER_SIZE];

    memset(input_buffer, 0, INPUT_BUFFER_SIZE);
    int input_pos = 0;
    int input_scroll = 0;
    int input_length = 0;
    int tail_pos = 0;

    // Initialize ncurses window
    initscr();

    // This is a macro to get height, width (hence no pointers)
    getmaxyx(stdscr, height, width);

    if (height < 3) {
        fprintf(stderr, "Not enough space in terminal for console!");
        endwin();
        return;
    }

    start_color();
    use_default_colors();
    cbreak();
    noecho();

    tail = create_tail_window(height, width);
    input = create_input_window(height, width);
    status = create_status_window(height, width);

    keypad(input, TRUE);

    wrefresh(tail);
    wrefresh(input);
    wrefresh(status);

    wmove(input, 0, 2);

    while ((curr = wgetch(input)) != KEY_F(2)) {

        switch (curr) {
            case KEY_BACKSPACE:
            case KEY_DC:
            case 127:
                // Backspace
                if (input_pos <= 0) {
                    break;
                }

                // Move text over from the right side
                if (input_pos < input_length) {
                    // Add 1 to length to also copy null terminator
                    memmove(input_buffer + input_pos - 1, input_buffer + input_pos,
                            sizeof(char) * (input_length - input_pos + 1));
                    //input_buffer[input_length] = '\0';
                    mvwprintw(input, 0, input_pos - input_scroll + 3, "%.*s",
                              MIN(width - 3, input_length - input_pos + 1),
                              input_buffer + input_pos);
                }

                input_buffer[input_length-1] = '\0';

                mvwdelch(input, 0, input_pos+1);
                input_pos--;
                input_length--;
                break;

            case KEY_ENTER:
            case 10:
                /*
                if (tail_pos == height - 3) {
                    scroll(tail);
                } else {
                    tail_pos++;
                }
                */
                send_data(data, input_buffer);

                memset(input_buffer, 0, INPUT_BUFFER_SIZE);
                wmove(input, 0, 2);
                input_pos = 0;
                input_scroll = 0;
                input_length = 0;
                wclrtoeol(input);

                break;

            case KEY_LEFT:
                if (input_pos <= 0) {
                    break;
                }

                input_pos--;
                wmove(input, 0, input_pos - input_scroll + 2);
                break;

            case KEY_RIGHT:
                // Don't go past the null terminator
                if (input_pos >= INPUT_BUFFER_LIMIT) {
                    break;
                }

                if (!input_buffer[input_pos]) {
                    break;
                }

                // Move ahead.
                input_pos++;
                wmove(input, 0, input_pos - input_scroll + 2);
                break;

            default:
                if (!isprint(curr)) {
                    break;
                }

                if (input_length >= INPUT_BUFFER_LIMIT) {
                    break;
                }

                if (input_pos < input_length) {
                    memmove(input_buffer + input_pos + 1, input_buffer + input_pos,
                            sizeof(char) * (input_length - input_pos));
                    mvwprintw(input, 0, 2, "%.*s", width - 3, input_buffer + input_scroll);
                }
                input_buffer[input_pos] = curr;
                mvwaddch(input, 0, input_pos - input_scroll + 2, curr);
                input_pos++;
                input_length++;
                break;

        }

        // scroll left and right...

        if (input_pos - input_scroll == width - 2) {
            // scroll the input to the left
            input_scroll++;
            mvwprintw(input, 0, 2, "%.*s", width - 3, input_buffer + input_scroll);
            mvwaddch(input, 0, width - 1, ' ');
        };

        if (input_pos - input_scroll < 0 && input_scroll > 0) {
            // scroll the input to the right
            input_scroll--;
            mvwprintw(input, 0, 2, "%.*s", width - 3, input_buffer + input_scroll);
            mvwaddch(input, 0, width - 1, ' ');
        };

        wmove(input, 0, 2 + input_pos - input_scroll);

        wclear(status);
        mvwprintw(status, 0, 0, "buffer index: %d // scroll: %d // len: %d",
                  input_pos, input_scroll, strlen(input_buffer));
        wrefresh(status);

        /*
        wmove(tail, tail_pos, 0);
        wclrtoeol(tail);
        wprintw(tail, input_buffer);
        wrefresh(tail);
        */
        wrefresh(input);
    }

    delwin(tail);
    delwin(input);
    delwin(status);
    endwin();

}

WINDOW *create_tail_window(int height, int width) {
    WINDOW *tail = newwin(height - 2, width, 0, 0);
    //idlok(tail, TRUE);
    scrollok(tail, TRUE);
    return tail;
}

WINDOW *create_input_window(int height, int width) {
    WINDOW *input = newwin(1, width, height - 2, 0);
    init_pair(1, COLOR_GREEN, -1);
    wattron(input, COLOR_PAIR(1));
    mvwprintw(input, 0, 0, ">");
    wattroff(input, COLOR_PAIR(1));
    return input;
}

WINDOW *create_status_window(int height, int width) {
    return newwin(1, width, height - 1, 0);
}

