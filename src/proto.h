#include <stddef.h>

#ifndef ICICLE_PROTO_H
#define ICICLE_PROTO_H

struct icicle_proto {
    char label;
    int (*handler)(char *data, size_t len, void *aux);
};

enum icicle_proto_err {
    OK = 0, // executed successfully
    ERR_NOCMD, // no command specified
    ERR_UNKNOWN // unknown command
};

/*
 * Given the following handler array, check if the data is matched
 * by any internal command. "aux" is passed to any called handler.
 * Returns values of enum icicle_proto_err.
 *
 * The "handlers" array is terminated by an element which has a null
 * ('\0') as its label.
 */
int handle_proto(const struct icicle_proto *handlers, char *data, size_t len, void *aux);

#endif
