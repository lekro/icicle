#ifndef ICICLE_SOCKET_H
#define ICICLE_SOCKET_H

struct sockaddr_un *get_sockaddr(const char *socket_path);
char *get_socket_path();

#endif
