#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "commands.h"
#include "main.h"

#include "daemon/daemon.h"
#include "client/shell.h"

static int print_help(int sock);

static const struct command COMMANDS[] = {
    {"daemon", start_daemon},
    {"shell", client_shell},
    {"help", print_help},
};

static const int COMMAND_COUNT = sizeof(COMMANDS) / sizeof(struct command);

void process_command(char *argv[], int sock) {

    if (!argv || !*argv) {
        fputs("No command specified!\n", stderr);
        return;
    }

    for (int i = 0; i < COMMAND_COUNT; i++) {
        if (strcmp(argv[0], COMMANDS[i].name) == 0) {
            COMMANDS[i].func(sock);
            return;
        }
    }

    fputs("Unknown command!\n", stderr);

}

static int print_help(int sock) {
    printf(get_help(), "icicle");
    return 0;
}
