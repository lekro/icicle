SOURCES = console.c path.c main.c config.c commands.c socket.c proto.c \
		  daemon/daemon.c client/shell.c daemon/daemon_proto.c \
		  daemon/server.c client/client_proto.c

SRCS := $(addprefix src/,$(SOURCES))
OBJS := $(addprefix build/,$(SOURCES:.c=.o))
OBJDIRS = $(sort $(dir $(OBJS)))

CLIBS = ncurses
CLIBS := $(addprefix -l,$(CLIBS)) -pthread

CFLAGS += -Wall -Wpedantic -g -D_DEBUG -pthread

all: icicle

build/%.o: src/%.c | $(OBJDIRS)
	gcc -c $< $(CFLAGS) -o $@

$(OBJDIRS):
	mkdir -p $@

icicle: $(OBJS)
	gcc $^ $(CLIBS) -o $@

clean:
	rm -rf build
	rm -f icicle

.PHONY: all clean

